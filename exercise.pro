TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    exercise/1-1main.cpp \
    exercise/1-4main.cpp \
    exercise/1-5main.cpp \
    exercise/1-6main.cpp \
    exercise/1-7main.cpp \
    exercise/1-8main.cpp \
    exercise/2-1main.cpp \
    exercise/2-2main.cpp \
    exercise/2-3main.cpp \
    exercise/2-4main.cpp \
    exercise/2-5main.cpp \
    exercise/2-6main.cpp \
    exercise/3-1main.cpp \
    exercise/3-2main.cpp \
    exercise/3-3main.cpp \
    exercise/3-4main.cpp \
    exercise/4-1main.cpp \
    exercise/4-2min.cpp \
    exercise/4-4main.cpp \
    exercise/4-5main.cpp \
    exercise/5-1main.cpp \
    exercise/5-2main.cpp \
    exercise/6-2main.cpp \
    exercise/7-2main.cpp \
    exercise/7-3main.cpp \
    exercise/Stack.cpp \
    KalmanFilters/calman.cpp \
    KalmanFilters/matrix.cpp \

HEADERS += \
    li.h \
    Stack.h \
    matrix1.h \
    example.h \
    globalwrapper.h \
    matrixo.h \
    stack.h \
    StackOI.h \
    stacktwo.h \
    userprofile.h \
    ../KalmanFilters/matrix.h \
    ../KalmanFilters/g.h \
    exercise/example.h \
    exercise/globalwrapper.h \
    exercise/li.h \
    exercise/matrix1.h \
    exercise/matrixo.h \
    exercise/stack.h \
    exercise/Stack.h \
    exercise/StackOI.h \
    exercise/stacktwo.h \
    exercise/userprofile.h \

SUBDIRS += \
    liuzang.pro \
    liuzang.pro \
    exercise/liuzang.pro \
    KalmanFilters/KalmanFilters.pro \
    KalmanFilters/KalmanFilters.pro

DISTFILES += \
    exercise/exercise.pro.user
