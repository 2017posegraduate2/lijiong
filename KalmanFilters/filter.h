#ifndef FILTER_H
#define FILTER_H
#include <iostream>
#include"matrix.h"

/**
 * @brief The Filter class定义一个实现卡尔曼滤波初始化、计算、输出的类
 */
class Filter
{
  public:
    Filter();//默认构造器
    ~Filter(){}//析构器
    void init_measure(float measurement[],int b);//初始化测量矩阵函数
    void init_R(float r1,float r2);
    void init_F(float dt);
    void init_P(float p1,float p2);
    void init_x(float x1,float y1);
    void filter(int a);
    void display();

  private:
    Matrix<float> filter_measurement_;//接收初始化的measurements,实际的x,y测量值
    Matrix<float> filter_initial_xy;//接收初始化的initial_xy初始x.y的位置
    Matrix<float> filter_x_;//接收初始化的x矩阵的第一个值表示X方向位置，第二个值为Y方向位置，第三个值为X方向速度，未知设为0，第四个值为Y方向速度，未知设为0
    Matrix<float> filter_u_;//接收初始化的u矩阵中表示外加的移动，比如前方车突然被别的车撞了，一般情况都设为0就行了
    Matrix<float> filter_P_;//接收初始化的P矩阵中，由于初始速度都未知，为v都设置为较大的不确定性，设为1000，参数都设置在对角线上
    Matrix<float> filter_F_;//接收初始化的F表示因为x（预）=x（当前）+v×∆t，让F矩阵乘x矩阵后，则分别得到下一次的x,y,以及他们的速度
    Matrix<float> filter_H_;//接收初始化的H测量矩阵，谁测量了谁为1
    Matrix<float> filter_R_;//接收初始化的R对应测量值的误差值，这个根据情况设定，这里设为0.1，和1
    float filter_dt_;//定义时间间隔


};

/**
 * @brief reverse定义了一个求2*2矩阵逆阵的方法
 * @param m参与转置的矩阵
 * @return
 */
inline Matrix<float> reverse(Matrix<float>&m)
{
  Matrix<float>rev(2,2);
  float p;
  p=m(0,0)*m(1,1)-m(0,1)*m(1,0);
  rev(0,0)=m(1,1)/p;
  rev(0,1)=(-m(1,0)/p);
  rev(1,0)=(-m(0,1)/p);
  rev(1,1)=(m(0,0)/p);
  return rev;
}

#endif // FILTER_H
