#include "filter.h"
#include<iostream>
using namespace std;

/**
 * @brief Filter::Filter初始化循环过程中固定不变的矩阵
 */
Filter::Filter()
{
  Matrix<float> H( 2, 4 );
  float gr[8]={1.,0.,0.,0.,0.,1.,0.,0.};
  for ( int i = 0, k = 0; i < 2; ++i )
    for ( int j = 0; j < 4; ++j )
      H( i, j ) = gr[ k++ ];
  filter_H_=H;

  /**
   * @brief 初始化u矩阵中表示外加的移动
   */
  Matrix<float> u( 4, 1 );
  float dr[4]={0.,0.,0.,0};
  for ( int i = 0, k = 0; i < 4; ++i )
    for ( int j = 0; j < 1; ++j )
      u( i, j ) = dr[ k++ ];
  filter_u_=u;
  cout << " u: " <<  filter_u_ << endl;
}

/**
 * @brief Filter::init_measure 初始化实际的x,y测量值
 * @param measurement实际的x,y测量值
 */
void Filter::init_measure(float measurement[],int b)
{
  Matrix<float> measurements( b, 2 );
  for ( int i = 0, k = 0; i < b; ++i )
    for ( int j = 0; j < 2; ++j )
      measurements( i, j ) = measurement[ k++ ];
  filter_measurement_=measurements;
}

/**
 * @brief Filter::init_R按照用户输入的数据对矩阵R进行定义
 * @param r1用户设定的x的误差值
 * @param r2用户设定的y的误差值
 */
void Filter::init_R(float r1, float r2)
{
  Matrix<float> R( 2, 2 );
  float hr[4]={r1,0.,0.,r2};
  for ( int i = 0, k = 0; i < 2; ++i )
    for ( int j = 0; j < 2; ++j )
      R( i, j ) = hr[ k++ ];
  filter_R_=R;
  cout << " R: " <<  filter_R_ << endl;
}

/**
 * @brief Filter::init_F 初始化变换矩阵
 * @param dt时间间隔
 */
void Filter::init_F(float dt)
{
  filter_dt_=dt;
  Matrix<float> F( 4, 4 );
  float fr[16]={1., 0., filter_dt_, 0., 0., 1., 0., filter_dt_,0., 0., 1.,0.,0.,0.,0.,1. };
  for ( int i = 0, k = 0; i < 4; ++i )
    for ( int j = 0; j < 4; ++j )
      F( i, j ) = fr[ k++ ];
  filter_F_=F;
  cout << " F: " <<  filter_F_ << endl;
}

/**
 * @brief Filter::init_P 初始化P矩阵
 * @param p1 vx较大的不确定性
 * @param p2 vy较大的不确定性
 */
void Filter::init_P(float p1, float p2)
{
  Matrix<float> P( 4, 4 );
  float er[16]={0., 0., 0., 0., 0., 0., 0., 0.,0., 0., p1,0.,0.,0.,0.,p2 };
  for ( int i = 0, k = 0; i < 4; ++i )
    for ( int j = 0; j < 4; ++j )
      P( i, j ) = er[ k++ ];
  filter_P_=P;
  cout << " P: " <<  filter_P_ << endl;
}

/**
 * @brief Filter::init_x矩阵的第一个值表示X方向位置，第二个值为Y方向位置，第三个值为X方向速度，未知设为0，第四个值为Y方向速度，未知设为0
 * @param x1 X方向位置
 * @param y1 Y方向位置
 */
void Filter::init_x(float x1, float y1)
{
  Matrix<float> x( 4, 1 );
  float cr[4]={x1,y1,0.,0.};
  for ( int i = 0, k = 0; i < 4; ++i )
    for ( int j = 0; j < 1; ++j )
      x( i, j ) = cr[ k++ ];
  filter_x_=x;
  cout << " x: " <<  filter_x_ << endl;

  /**
   * @brief initial_xy初始位置
   */
  Matrix<float> initial_xy( 1, 2 );
  float br[2]={x1,y1};
  for ( int i = 0, k = 0; i < 1; ++i )
    for ( int j = 0; j < 2; ++j )
      initial_xy( i, j ) = br[ k++ ];
  filter_initial_xy=initial_xy;
  cout << " initial_xy: " <<  filter_initial_xy << endl;
}


/**
 * @brief Filter::filter卡尔曼滤波的具体算法
 */
void Filter::filter(int a)
{
  Matrix<float> y(filter_H_.rows(),filter_x_.cols());
  Matrix<float> Z(1,2);//每次measurement关于x.y的测量值
  Matrix<float> K(filter_P_.rows(),filter_R_.cols());
  Matrix<float> S(filter_R_.rows(),filter_R_.cols());
  Matrix<float>Q(filter_H_.rows(),filter_x_.cols());
  Matrix<float> SS(filter_R_.rows(),filter_R_.cols());
  Matrix<float>L(filter_F_.cols(),filter_F_.rows());
  L.transpose(filter_F_);
  Matrix<float>M(filter_H_.cols(),filter_H_.rows());
  M.transpose(filter_H_);
  vector<float> z_array(2);

  for ( int m = 0; m < a ; ++m )
  {
    Matrix<float> I( 4, 4 );
    float ir[16]={1., 0., 0., 0., 0., 1., 0., 0.,0., 0., 1.,0.,0.,0.,0.,1. };
    for ( int i = 0, k = 0; i < 4; ++i )
      for ( int j = 0; j < 4; ++j )
        I( i, j ) = ir[ k++ ];
    /**运动更新(预测)**/
    filter_P_=(filter_F_*filter_P_)*L;
    filter_x_=filter_F_*filter_x_+filter_u_;
    /**对测量值的更新，得到滤波后的测量值**/
    z_array[0] = filter_measurement_(m, 0);
    z_array[1] = filter_measurement_(m, 1);
    Z.setElem(z_array);
    Q.transpose(Z);
    y=Q-(filter_H_*filter_x_);/**运动更新后的位置与测量值的偏差**/
    S=(filter_H_*filter_P_*M)+filter_R_;/**R具体指的是测量噪声**/
    SS=reverse(S);
    K=filter_P_*M*SS;/**K具体是指卡尔曼增益**/
    filter_x_=filter_x_+(K*y);
    filter_P_=(I-(K*filter_H_))*filter_P_;
  }
}

/**
 * @brief Filter::display输出计算到第k次的结果
 */
void Filter::display()
{
  cout<<"x:"<<filter_x_<<endl;
  cout<<"P:"<<filter_P_<<endl;
}







