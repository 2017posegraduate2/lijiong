#include <iostream>
#include"matrix.h"
#include"filter.h"
#include"g.h"

void calmanfilter()
{
  float measurement[12]={5., 10., 6., 8., 7., 6., 8., 4.,9., 2., 10.,0.};
  /**
   * @brief calmanFilter定义卡尔曼滤波对象
   */
  Filter calmanFilter;
  calmanFilter.init_measure(measurement,6);//初始化用户设定的实际测量值
  calmanFilter.init_R(0.1,1.);//用户设定的x与y的误差值
  calmanFilter.init_F(0.1);//两次测量之间的时间间隔
  calmanFilter.init_P(1000.,1000.);//x与y方向上初始速度的不确定值
  calmanFilter.init_x(4.,12.);//x与y方向上初始位置
  calmanFilter.filter(6);//调用滤波算法计算第6次的数值
  calmanFilter.display();//调用滤波算法类输出第6次的结果
}


