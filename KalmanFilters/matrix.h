#ifndef MATRIX_H
#define MATRIX_H
/**************************************************
 *Class Matrix

**************************************************/
#include <iostream>
#include <cassert>
#include<algorithm>
#include <math.h>
using namespace std;

/**@brief 定义矩阵的泛型模板类
 * 实现矩阵的加、减、乘、转置运算
 */
template <typename elemType>
class Matrix
{
    template<typename T>
    friend Matrix<T> operator*( const Matrix<T>&, const Matrix<T>& );

  public:
    Matrix( int rows, int columns );
    Matrix( const Matrix& );
    Matrix( ){_matrix = NULL;}

    /**
     * @brief operator =(析构函数，释放内存)
     * @return
     *
     */
    ~Matrix(){ delete [] _matrix; }
    
    Matrix& operator=( const Matrix& );
    Matrix& operator-( const Matrix& );
    Matrix& operator+( const Matrix& );
    void operator+=( const Matrix& );
    
    /**
     * @brief operator (定义借助一维数组访问矩阵索引元素的方法)
     * @param row 索引元素所在矩阵的行数
     * @param column 索引元素所在矩阵的列数
     * @return
     */
    elemType& operator()( int row, int column )
    { return _matrix[ row * cols() + column ]; }
    
    const elemType& operator()( int row, int column ) const
    { return _matrix[ row * cols() + column ]; }
    
    /**
     * @brief 访问矩阵内行列数值
     * @return
     */
    int rows() const { return rows_; }
    int cols() const { return cols_; }
    
    /**
     * @brief same_size 矩阵相加时进行行列值大小相等判断
     * @param m:参与加法的矩阵
     * @return
     */
    bool same_size( const Matrix &m ) const
    { return rows() == m.rows() && cols() == m.cols(); }
    
    /**
     * @brief comfortable矩阵相乘时进行第一个矩阵列数与第二个矩阵行数的判断
     * @param m:参与乘法的矩阵
     * @return
     */
    bool comfortable( const Matrix &m ) const
    { return ( cols() == m.rows() ); }
    
    ostream& print( ostream& ) const;
    Matrix& transpose(const Matrix &m );
    Matrix& cholesky ( );
    void setElem(vector<elemType> elem);
    
  protected:
    int  rows_;
    int  cols_;
    elemType *_matrix;
};

/**
 *@brief 内联函数：对输出运算符进行重载，实现矩阵的输出
 */
template <typename elemType>
inline ostream& operator<<( ostream& os, const Matrix<elemType> &m )
{ return m.print( os ); }

/**
 *@brief 对矩阵加法进行重载
 */
template <typename elemType>
Matrix<elemType>& Matrix<elemType>::operator+( const Matrix&m )
{
  assert( same_size( m )  != 0 );
  register int matrix_size = cols() * rows();
  for ( int ix = 0; ix < matrix_size; ++ix )
    ( *( _matrix + ix )) += ( *( m._matrix + ix ));
  return *this;
}

/**
 *@brief 获取每次的测量值
 */
template <typename elemType>
void Matrix<elemType>::setElem(vector<elemType> elem)
{
  for(uint i = 0; i < elem.size(); ++i)
  {
    *(_matrix + i) = elem[i];
  }

}

/**
 *@brief 对矩阵乘法进行重载
 */
template <typename elemType>
Matrix<elemType>operator*( const Matrix<elemType> &m1, const Matrix<elemType> &m2 )
{
  assert( m1.comfortable( m2 ) != 0 );
  Matrix<elemType> result( m1.rows(), m2.cols() );

  for ( int ix = 0; ix < m1.rows(); ix++ )
  {
    for ( int jx = 0; jx < m1.cols(); jx++ )
    {
      result( ix, jx ) = 0;
      for ( int kx = 0; kx < m1.cols(); kx++ )
        result( ix, jx ) += m1( ix, kx ) * m2( kx, jx );
    }
  }
  return result;
}

/**
 *@brief 对矩阵减法进行重载
 */
template <typename elemType>
Matrix<elemType>& Matrix<elemType>::operator-( const Matrix&m )
{
  assert( same_size( m )  != 0 );
  register int matrix_size = cols() * rows();

  for ( int ix = 0; ix < matrix_size; ++ix )
    ( *( _matrix + ix )) =( *( _matrix + ix ))-( *( m._matrix + ix ));
  return *this;
}

template <typename elemType>void Matrix<elemType>::operator+=( const Matrix &m )
{
  assert( same_size( m ) != 0 );
  register int matrix_size = cols() * rows();

  for ( int ix = 0; ix < matrix_size; ++ix )
    ( *( _matrix + ix )) += ( *( m._matrix + ix ));
}

/**
 *@brief 矩阵中转置方法的定义
 */
template <typename elemType>
Matrix<elemType>& Matrix<elemType>::transpose(const Matrix &m)
{

  for ( int ix = 0; ix < m.rows(); ix++ )
  {
    for ( int jx = 0; jx < m.cols(); jx++ )
    {
      int t=ix*m.cols()+jx;
      int k=jx*this->cols() + ix;
      _matrix[k]=m._matrix[t];
    }
  }
  return *this;
}


template <typename elemType>
Matrix<elemType>& Matrix<elemType>::cholesky( )
{
  int n=_matrix.rows();
  Matrix<elemType> rever( _matrix.rows(), _matrix.cols() );
  for(int i=1;i<=n;i++)
  {
    rever(i,i)=_matrix(i,i);
    for(int k=1;k<=i-1;k++)
    {
      rever(i,i)=rever(i,i)-rever(k,i)*rever(k,i);
    }
    rever(i,i)=sqrt(rever(i,i));
    for(int j=i+1;j<=n;j++)
    {
      rever(i,j)=_matrix(i,j);
      for(int k=1;k<=i-1;k++)
      {
        rever(i,j)=rever(i,j)-rever(k,i)*rever(k,j);
      }
      rever(i,j)=rever(i,j)/rever(i,i);
    }
  }
  return rever;
}

/**
 *@brief 矩阵输出方法的定义
 */
template <typename elemType>
ostream& Matrix<elemType>::print( ostream &os ) const
{
  register int col = cols();
  register int matrix_size = col * rows();

  for ( int ix = 0; ix < matrix_size; ++ix )
  {
    if ( ix % col == 0 ) os << endl;
    os << ( *( _matrix + ix )) << ' ';
  }

  os << endl;
  return os;
}

/**
 *@brief 矩阵初始化方法：将大小相同的矩阵数值传入
 */
template <typename elemType>
Matrix<elemType>::Matrix( const Matrix & rhs )
{
  rows_ = rhs.rows_; cols_ = rhs.cols_;
  int mat_size = rows_ * cols_;
  _matrix = new elemType[ mat_size ];
  for ( int ix = 0; ix < mat_size; ++ix )
    _matrix[ ix ] = rhs._matrix[ ix ];

}

/**
 *@brief 重载矩阵需要使用的等号
 */
template <typename elemType>
Matrix<elemType>& Matrix<elemType>::operator=( const Matrix &rhs )
{
  if ( this != &rhs )
  {
    rows_ = rhs.rows_; cols_ = rhs.cols_;
    int mat_size = rows_ * cols_;
    if(_matrix)
    {
      delete [] _matrix;
      _matrix = NULL;
    }
    _matrix = new elemType[ mat_size ];
    for ( int ix = 0; ix < mat_size; ++ix )
      _matrix[ ix ] = rhs._matrix[ ix ];
  }
  return *this;
}

/**
 *@brief 借助行列大小对矩阵的初始化方法
 */
template <typename elemType>
Matrix<elemType>::Matrix( int rows, int columns )
  : rows_( rows ), cols_( columns )
{
  int size = rows_ * cols_;
  _matrix = new elemType[ size ];
  for ( int ix = 0; ix < size; ++ix )
    _matrix[ ix ] = elemType();
}
#endif // MATRIX_H
