TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    calman.cpp \
    matrix.cpp

HEADERS += \
    matrix.h \
    g.h
