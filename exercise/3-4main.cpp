#include <algorithm>
#include <vector>
#include <iterator>
#include <fstream>
#include <iostream>
#include "li.h"
using namespace std;

class even_elem{
  public:
    bool operator()(int elem)
    {return elem%2 ? false :true;}
};

int ex3_4(){

  istream_iterator<int> in(cin),eos;
  vector<int> input;
  copy(in,eos,back_inserter(input));
  vector<int>::iterator division=partition(input.begin(),input.end(),even_elem());
  ofstream even_file("/home/lijiong/XUEXI/even_file"),
      odd_file("/home/lijiong/XUEXI/odd_file");
  if(!even_file)
  {
    cerr<<"Arghh! unable to open even_file--bailing out!\n";
    return -1;
  }
  if(!odd_file){
    cerr<<"Arghh! unable to open odd_file--bailing out!\n";
    return -1;
  }
  ostream_iterator<int> even_iter(even_file,"\n"),
      odd_iter(odd_file," ");
  copy(input.begin(),division,even_iter);
  copy(division,input.end(),odd_iter);//*need input '/n' *//


}
