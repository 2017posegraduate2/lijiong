#ifndef STACKTWO_H
#define STACKTWO_H

#include <iostream>
#include <string>
#include <vector>


using namespace std;

typedef string elemType;
class Stacktwo
{
  public:
    Stacktwo(int capacity=0):_top(0)
    {if(capacity) _stacktwo.reserve(capacity);}

    virtual ~Stacktwo(){}
    int size() const{ return _stacktwo.size();}
    bool empty() const {return !_top;}
    bool full() const {return size() >= _stacktwo.max_size();}
    int top() const {return _top;}
    void print(ostream &os=cout) const;

    bool pop(elemType&);
    bool push(const elemType&);
    virtual bool peek(int ,elemType&){return false;}
  protected:
    vector<elemType> _stacktwo;
    int _top;
};

ostream& operator<<(ostream &os, const Stacktwo &rhs)
{
  rhs.print();
  return os;
}

bool Stacktwo::pop(elemType &elem)
{
  if(empty()) return false;
  elem=_stacktwo[--_top];
  _stacktwo.pop_back();
  return true;
}

bool Stacktwo::push(const elemType &elem)
{
  if(full()) return false;
  _stacktwo.push_back(elem);
  ++_top;
  return true;
}
void Stacktwo::print(ostream &os)const
{
  vector<elemType>::const_reverse_iterator rit=_stacktwo.rbegin(),rend=_stacktwo.rend();
  os<<"\n\t";
  while(rit!=rend)
    os<<*rit++<<"\n\t";
  os<<endl;
}

class Peekback_Stack:public Stacktwo
{
  public:
    Peekback_Stack(int capacity=0):Stacktwo(capacity){}
    virtual bool peek(int index,elemType &elem);

};
bool Peekback_Stack::peek(int index,elemType &elem)
{
  if(empty()) return false;
  if(index<0||index>=size())
    return false;
  elem= _stacktwo[index];
  return true;
}


#endif // STACKTWO_H
