#include <iostream>
#include <string>
#include "Stack.h"
#include "li.h"


int ex4_2( )
{
  Stack st;
  string str;
  while(cin>>str&&!st.full())
  {if(str=="q")break;//add an end mark
    st.push(str);
  }
  if(st.empty()){
    cout<<'\n'<<"Oops:no strings were read--biling out\n";
    return 0;

  }
  st.peek(str);
  if(st.size()==1&&str.empty()){
    cout<<'\n'<<"Opp : no strings were read--bailing out\n ";
    return 0;
  }

  cout<<'\n'<<"read in "<<st.size()<<"strings!\n";
  cin.clear();
  cout<<"what word to search for ? ";
  cin>> str;
  bool found =st.find(str);
  int count=found?st.count(str):0;
  cout<<str<<(found?" is ":"isn\'t")<<" in the stack. ";
  if(found)
    cout<<" It occurs "<<count<<" times\n ";
}

