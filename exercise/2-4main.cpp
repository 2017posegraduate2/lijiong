#include <iostream>
#include <vector>
#include"li.h"
using namespace std;
inline bool check_validity(int pos)
{
  return ((pos<=0||pos>64)?false:true);
}
const vector<int> *
pentgonal_series(int pos)
{
  static vector<int>_elems;
  if (check_validity(pos)&&(pos>_elems.size()))
  {
    for(int ix=_elems.size()+1;ix<=pos;++ix)
      _elems.push_back((ix*(3*ix-1))/2);
  }
  return &_elems;
}
bool pentgonal_elem(int pos,int &elem)
{
  if(!check_validity(pos))
  {
    cout<<"Sorry,your number"<<pos<<" is invalid."<<endl;
    elem=0;
    return false;
  }
  const vector<int>*pent = pentgonal_series(pos);
  elem = (*pent)[pos-1];
  return true;
}

int ex2_4(  )
{
  int elem;
  if(pentgonal_elem(8,elem))
    cout << "Element 8 is "<<elem<<'\n';
  if(pentgonal_elem(88,elem))
    cout << "Element 88 is "<<elem<<'\n';
  if(pentgonal_elem(15,elem))
    cout << "Element 25 is "<<elem<<'\n';
  if(pentgonal_elem(58,elem))
    cout << "Element 58 is "<<elem<<'\n';
  return 0;
}
