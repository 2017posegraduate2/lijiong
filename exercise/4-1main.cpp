#include <iostream>
#include <string>
#include "Stack.h"
#include "li.h"


int ex4_1( )
{
  Stack st;
  string str;
  while(cin>>str&&!st.full())
  {
    if(str=="q")break;
    st.push(str);
  }
  if(st.empty()){
    cout<<'\n'<<"Oops:no strings were read--biling out\n";
    return 0;

  }
  st.peek(str);
  if(st.size()==1&&str.empty()){
    cout<<'\n'<<"Opp : no strings were read--bailing out\n ";
    return 0;
  }

  cout<<'\n'<<"read in "<<st.size()<<"strings!\n"<<"The strings, in reverse order:\n";
  while(st.size())
    if(st.pop(str))
      cout<<str<<' ';
  cout<<'\n'<<"There are now "<<st.size()<<" elements in the stack!\n";

}
