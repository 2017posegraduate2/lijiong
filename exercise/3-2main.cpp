#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include "li.h"

using namespace std;
template<typename elemtype>
void display_vector(const vector<elemtype>&vec,ostream &os=cout,int len=8)
{typename std::vector<elemtype>::const_iterator iter=vec.begin(),
      end_it=vec.end();
  int elem_cnt=1;
  while(iter!=end_it)
    os<<*iter++<<(!(elem_cnt++%len)?'\n':' ');
  os<<endl;}
class LessThan{
  public:
    bool operator()(
        const string &s1,const string &s2){
      return s1.size()<s2.size();}
};//define a function object//


int ex3_2()
{
  ifstream ifile("/home/lijiong/XUEXI/san");
  ofstream ofile("/home/lijiong/XUEXI/sam.map");
  if(!ifile)
  {
    cerr<<"Unable to open ifile--bailing out!\n";
    return -1;
  }
  if(!ofile){
    cerr<<"Unable to open ofile--bailing out!\n";
    return -1;
  }
  vector<string>text;
  string word;
  while(ifile>>word)
    text.push_back(word);
  sort(text.begin(),text.end(),LessThan());
  display_vector(text,ofile);//write to sam.map//
  display_vector(text);//diaplay to computer//
}

