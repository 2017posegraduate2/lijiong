#include <iostream>
#include <string>
#include <vector>
#include "li.h"
#include "stacktwo.h"

using namespace std;

void peek(Stacktwo &st,int index)
{
  cout<<endl;
  string t;
  if(st.peek(index,t))
    cout<<"peek:"<<t;
  else cout<<"peek failed!";
  cout<<endl;
}


int ex5_2( )
{
  Stacktwo st;
  string str;
  while(cin>>str&&!st.full())
  {
    if(str=="q") break;
    st.push(str);
  }
  cout<<'\n'<<"About to call peek() with Stacktwo"<<endl;
  peek(st,st.top()-1);
  cout<<st;

  Peekback_Stack pst;
  while(!st.empty())
  {
    string t;
    if(st.pop(t))
      pst.push(t);
  }
  cout<<"About to call peek() with Peekback_Stack"<<endl;
  peek(pst,pst.top()-1);
  cout<<pst;

  return 0;
}
