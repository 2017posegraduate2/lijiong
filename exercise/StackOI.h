#ifndef STACKTWO_H
#define STACKTWO_H

#include <iostream>
#include <string>
#include <vector>
#include<exception>

using namespace std;

typedef string elemType;
class Stacktwo
{
  public:
    Stacktwo(int capacity=0):_top(0)
    {if(capacity) _stacktwo.reserve(capacity);}

    virtual ~Stacktwo(){}
    int size() const{ return _stacktwo.size();}
    bool empty() const {return !_top;}
    bool full() const {return size() >= _stacktwo.max_size();}
    int top() const {return _top;}
    void print(ostream &os=cout) const;

    void pop(elemType&);
    void push(const elemType&);
    virtual bool peek(int ,elemType&){return false;}
  protected:
    vector<elemType> _stacktwo;
    int _top;
};

ostream& operator<<(ostream &os, const Stacktwo &rhs)
{
  rhs.print();
  return os;
}
/**maybe be exception
 * @brief Stacktwo::pop
 * @param elem
 */
void Stacktwo::pop(elemType &elem)
{
  if(empty()) throw PopOnEmpty();
  elem=_stacktwo[--_top];
  _stacktwo.pop_back();
}

void Stacktwo::push(const elemType &elem)
{
  if(full())
  {
    _stacktwo.push_back(elem);
    ++_top;
    return ;
  }
  throw PushOnFull();
}
void Stacktwo::print(ostream &os)const
{
  vector<elemType>::const_reverse_iterator rit=_stacktwo.rbegin(),rend=_stacktwo.rend();
  os<<"\n\t";
  while(rit!=rend)
    os<<*rit++<<"\n\t";
  os<<endl;
}
/**define exception class
 * @brief The Peekback_Stack class
 */
class Peekback_Stack:public Stacktwo
{
  public:
    Peekback_Stack(int capacity=0):Stacktwo(capacity){}
    virtual bool peek(int index,elemType &elem);

};
bool Peekback_Stack::peek(int index,elemType &elem)
{
  if(empty()) return false;
  if(index<0||index>=size())
    return false;
  elem= _stacktwo[index];
  return true;
}

class StackException:public logic_error
{
  public:
    StackException(const char *what):_what(what){}
    const char *what() const{return _what.c_str();}
  protected:
    string _what;
};
class PopOnEmpty:public StackException
{
  public:
    PopOnEmpty():StackException("Pop on empty Stack "){}
};
class PushOnFull:public StackException
{
  public:
    PushOnFull():StackException("Push on full Stack "){}
};
#endif // STACKTWO_H
