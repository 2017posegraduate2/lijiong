#include <iostream>
#include <iomanip>
#include <cstring>
#include<exercise//li.h>
using namespace std;

int ex1_5( )
{
  const int nm_size=128;
  char user_name[nm_size];
  cout << "Please enter your name" << endl;
  cin >>setw(nm_size)>>user_name;
  switch( strlen(user_name) )
  {
    case 0:
      cout <<"Ah,there is no name!";
      break;
    case 1:
      cout <<"A 1-character name,"<<"Please enter your name again!";
      break;
    case 127:
      cout <<"Your name so long!"<<"please use a short name !";
    default:
      cout <<"Hello,"<<user_name<<"--happy to make your acquaintance!\n";
      break;
  }

  return 0;
}
